# frozen_string_literal: true

require_relative 'community_processor'

module Triage
  class AutomatedReviewRequestGeneric < CommunityProcessor
    WORKFLOW_READY_FOR_REVIEW_LABEL = 'workflow::ready for review'

    react_to 'merge_request.update'

    def applicable?
      wider_community_author_open_resource?(for_gitlab_org: true) &&
        workflow_ready_for_review_added? &&
        event.wip?
    end

    def process
      post_review_request_comment
    end

    private

    def workflow_ready_for_review_added?
      event.added_label_names.include?(WORKFLOW_READY_FOR_REVIEW_LABEL)
    end

    def post_review_request_comment
      add_comment('/draft')
    end
  end
end
