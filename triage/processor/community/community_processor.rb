# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/unique_comment'

module Triage
  class CommunityProcessor < Processor
    def wider_community_author?(for_gitlab_org: false, for_gitlab_com: false)
      gitlab_org_boolean = event.from_gitlab_org? && event.wider_community_author?
      gitlab_com_boolean = event.from_gitlab_com? && event.wider_gitlab_com_community_author?

      return gitlab_org_boolean || gitlab_com_boolean if for_gitlab_org && for_gitlab_com

      result = false
      result = gitlab_org_boolean if for_gitlab_org
      result = gitlab_com_boolean if for_gitlab_com
      result
    end

    def wider_community_author_open_resource?(for_gitlab_org: false, for_gitlab_com: false)
      event.resource_open? && wider_community_author?(for_gitlab_org: for_gitlab_org, for_gitlab_com: for_gitlab_com)
    end

    def wider_community_command?(for_gitlab_org: false, for_gitlab_com: false, by_resource_author: false)
      result = wider_community_author?(for_gitlab_org: for_gitlab_org, for_gitlab_com: for_gitlab_com) &&
        command.valid?(event)

      if by_resource_author
        result &&= event.by_noteable_author?
      end

      result
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
