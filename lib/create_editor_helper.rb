# frozen_string_literal: true

require_relative 'team_member_select_helper'

module CreateEditorHelper
  include TeamMemberSelectHelper

  BACKEND_TEAM = "Create:Editor BE Team"
  FRONTEND_TEAM = "Create:Editor FE Team"

  def create_editor_be
    @create_editor_be ||= select_random_team_member(BACKEND_TEAM)
  end

  def create_editor_fe
    @create_editor_fe ||= select_random_team_member(FRONTEND_TEAM)
  end
end
