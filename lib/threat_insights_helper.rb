# frozen_string_literal: true

require_relative 'team_member_select_helper'

module ThreatInsightsHelper
  include TeamMemberSelectHelper

  BACKEND_TEAM = "Secure:Threat Insights BE Team"
  FRONTEND_TEAM = "Secure:Threat Insights FE Team"

  def threat_insights_be
    @threat_insights_be ||= select_random_team_member(BACKEND_TEAM, include_ooo: false)
  end

  def threat_insights_fe
    @threat_insights_fe ||= select_random_team_member(FRONTEND_TEAM, include_ooo: false)
  end
end
