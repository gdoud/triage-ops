# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/code_review_experience_feedback'

RSpec.describe Triage::CodeReviewExperienceFeedback do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        project_id: project_id,
        iid: merge_request_iid
      }
    end
  end

  include_context 'with merge request notes'

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.merge', 'merge_request.close']

  describe '#applicable?' do
    let(:wider_community_author) { true }

    before do
      allow(subject).to receive(:wider_community_author?).with(for_gitlab_org: true, for_gitlab_com: true).and_return(wider_community_author)
    end

    context 'when event project is not under gitlab-org nor gitlab-com, or author is not from the wider community, or resource is not open' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when there is already a comment for the same purpose' do
      let(:merge_request_notes) do
        [
          { body: 'review comment 1' },
          { body: comment_mark }
        ]
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts code review experience message' do
      body = <<~MARKDOWN.chomp
        #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
        @#{event.resource_author.username}, how was your code review experience with this merge request? Please tell us how we can continue to [iterate](https://about.gitlab.com/handbook/values/) and improve:

        1. Leave a :thumbsup: or a :thumbsdown: on this comment to describe your experience.
        1. Create a new comment starting with `@gitlab-bot feedback` below, and leave any additional feedback you have for us in the comment.
        
        Have five minutes? Take our [survey](https://forms.gle/g26h8uEKgvTLGSQw6) to give us even more feedback on how GitLab can improve the contributor experience.

        Thanks for your help! :heart:
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
