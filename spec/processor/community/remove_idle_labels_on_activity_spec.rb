# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/remove_idle_labels_on_activity'

RSpec.describe Triage::RemoveIdleLabelOnActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        by_noteable_author?: by_noteable_author
      }
    end

    let(:by_noteable_author) { true }
    let(:label_names) { [described_class::IDLE_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.update', 'merge_request.note']

  describe '#applicable?' do
    let(:wider_community_author) { true }

    before do
      allow(subject).to receive(:wider_community_author?).with(for_gitlab_org: true).and_return(wider_community_author)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org, or author is not from the wider community' do
      let(:wider_community_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when the comment is not from the resource author' do
      let(:by_noteable_author) { false }

      include_examples 'event is not applicable'
    end

    context 'when there is no idle or stale label' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when event is an MR update' do
      include_context 'with event', 'Triage::MergeRequestEvent' do
        let(:event_attrs) do
          {
            from_gitlab_org?: true,
            by_noteable_author?: true,
            wider_community_author?: true,
            revision_update?: revision_update
          }
        end
      end

      let(:label_names) { [described_class::IDLE_LABEL] }

      context 'when MR update event is a revision update' do
        let(:revision_update) { true }

        include_examples 'event is applicable'
      end

      context 'when MR update event is not a revision update' do
        let(:revision_update) { false }

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a message to remove the labels' do
      expected_message =<<~MARKDOWN.chomp
        /unlabel ~"#{described_class::IDLE_LABEL}" ~"#{described_class::STALE_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
