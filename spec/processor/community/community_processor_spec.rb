# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/community_processor'

RSpec.describe Triage::CommunityProcessor do
  using RSpec::Parameterized::TableSyntax

  describe '#wider_community_author?' do
    describe 'with default arguments' do
      where(:from_gitlab_org, :wider_community_author, :from_gitlab_com, :wider_gitlab_com_community_author, :expected_result) do
        # When from_gitlab_org: true and wider_community_author: true => false
        true | true | false | false | false

        # When from_gitlab_org: false or wider_community_author: false => false
        false | true | false | false | false
        true | false | false | false | false

        # When from_gitlab_com: true and wider_gitlab_com_community_author: true => false
        false | false | true | true | false

        # When from_gitlab_com: false or wider_gitlab_com_community_author: false => false
        false | false | false | true | false
        false | false | true | false | false
      end

      with_them do
        include_context 'with event', 'Triage::MergeRequestEvent' do
          let(:event_attrs) do
            {
              from_gitlab_org?: from_gitlab_org,
              from_gitlab_com?: from_gitlab_com,
              wider_community_author?: wider_community_author,
              wider_gitlab_com_community_author?: wider_gitlab_com_community_author
            }
          end
        end

        subject { described_class.new(event) }

        it 'returns the expected result' do
          expect(subject.wider_community_author?).to eq(expected_result)
        end
      end
    end

    describe 'with for_gitlab_org: true' do
      where(:from_gitlab_org, :wider_community_author, :from_gitlab_com, :wider_gitlab_com_community_author, :expected_result) do
        # When from_gitlab_org: true and wider_community_author: true => true
        true | true | false | false | true

        # When from_gitlab_org: false or wider_community_author: false => false
        false | true | false | false | false
        true | false | false | false | false

        # When from_gitlab_org: false and wider_community_author: false => false
        false | false | true | true | false
        false | false | false | true | false
        false | false | true | false | false
      end

      with_them do
        include_context 'with event', 'Triage::MergeRequestEvent' do
          let(:event_attrs) do
            {
              from_gitlab_org?: from_gitlab_org,
              from_gitlab_com?: from_gitlab_com,
              wider_community_author?: wider_community_author,
              wider_gitlab_com_community_author?: wider_gitlab_com_community_author
            }
          end
        end

        subject { described_class.new(event) }

        it 'returns the expected result' do
          expect(subject.wider_community_author?(for_gitlab_org: true)).to eq(expected_result)
        end
      end
    end

    describe 'with for_gitlab_com: true' do
      where(:from_gitlab_org, :wider_community_author, :from_gitlab_com, :wider_gitlab_com_community_author, :expected_result) do
        # When from_gitlab_com: false and wider_gitlab_com_community_author: false => false
        true | true | false | false | false
        false | true | false | false | false
        true | false | false | false | false

        # When from_gitlab_com: true and wider_gitlab_com_community_author: true => true
        false | false | true | true | true

        # When from_gitlab_com: false or wider_gitlab_com_community_author: false => false
        false | false | false | true | false
        false | false | true | false | false
      end

      with_them do
        include_context 'with event', 'Triage::MergeRequestEvent' do
          let(:event_attrs) do
            {
              from_gitlab_org?: from_gitlab_org,
              from_gitlab_com?: from_gitlab_com,
              wider_community_author?: wider_community_author,
              wider_gitlab_com_community_author?: wider_gitlab_com_community_author
            }
          end
        end

        subject { described_class.new(event) }

        it 'returns the expected result' do
          expect(subject.wider_community_author?(for_gitlab_com: true)).to eq(expected_result)
        end
      end
    end
  end

  describe '#wider_community_author_open_resource?' do
    where(:wider_community_author, :resource_open, :expected_result) do
      # When wider_community_author: true and resource_open: true => true
      true | true | true

      # When wider_community_author: false => false
      false | true | false

      # When wider_community_author: true and resource_open: false => false
      true | false | false
    end

    with_them do
      include_context 'with event', 'Triage::MergeRequestEvent' do
        let(:event_attrs) do
          {
            resource_open?: resource_open
          }
        end
      end

      subject { described_class.new(event) }

      before do
        allow(subject).to receive(:wider_community_author?).and_return(wider_community_author)
      end

      it 'returns the expected result' do
        expect(subject.wider_community_author_open_resource?).to eq(expected_result)
      end
    end
  end

  describe '#wider_community_command?' do
    let(:processor) do
      Class.new(Triage::CommunityProcessor) do
        define_command name: 'foo'
      end
    end

    subject { processor.new(event) }

    before do
      allow(subject).to receive(:wider_community_author?).and_return(wider_community_author)

      command_stub = double(valid?: command_valid)
      allow(subject).to receive(:command).and_return(command_stub)
    end

    describe 'with default arguments' do
      where(:wider_community_author, :command_valid, :expected_result) do
        # When wider_community_author: true and command_valid: true => true
        true | true | true

        # When wider_community_author: false => false
        false | true | false

        # When wider_community_author: true and command_valid: false => false
        true | false | false
      end

      with_them do
        include_context 'with event', 'Triage::MergeRequestEvent'

        it 'returns the expected result' do
          expect(subject.wider_community_command?).to eq(expected_result)
        end
      end
    end

    describe 'with by_resource_author: true' do
      where(:wider_community_author, :command_valid, :by_resource_author, :expected_result) do
        # When wider_community_author: true and command_valid: true and by_resource_author: true => true
        true | true | true | true

        # When wider_community_author: false => false
        false | true | true | false

        # When wider_community_author: true and command_valid: false => false
        true | false | true | false

        # When by_resource_author: false => false
        true | true | false | false
      end

      with_them do
        include_context 'with event', 'Triage::MergeRequestEvent' do
          let(:event_attrs) do
            {
              by_noteable_author?: by_resource_author
            }
          end
        end

        it 'returns the expected result' do
          expect(subject.wider_community_command?(by_resource_author: true)).to eq(expected_result)
        end
      end
    end
  end
end
