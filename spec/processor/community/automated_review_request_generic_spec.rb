# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_generic'

RSpec.describe Triage::AutomatedReviewRequestGeneric do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        wip?: draft
      }
    end
    let(:draft) { true }
    let(:added_label_names) { [described_class::WORKFLOW_READY_FOR_REVIEW_LABEL] }
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ["merge_request.update"]

  describe '#applicable?' do
    let(:wider_community_author_open_resource) { true }

    before do
      allow(subject).to receive(:wider_community_author_open_resource?).with(for_gitlab_org: true).and_return(wider_community_author_open_resource)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org, or author is not from the wider community, or resource is not open' do
      let(:wider_community_author_open_resource) { false }

      include_examples 'event is not applicable'
    end

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when resource is not in draft' do
      let(:draft) { false }

      include_examples 'event is not applicable'
    end
  end

  describe '#process' do
    shared_examples 'process merge request review request' do
      it 'posts a comment mentioning an MR coach to request a review' do
        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when the MR is a draft' do
      let(:draft) { true }

      it_behaves_like 'process merge request review request' do
        let(:body) { '/draft' }
      end
    end
  end
end
