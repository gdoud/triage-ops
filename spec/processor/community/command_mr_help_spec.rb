# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_help'

RSpec.describe Triage::CommandMrHelp do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot help)
      }
    end
  end

  let(:roulette) do
    [
      { 'username' => 'coach', 'merge_request_coach' => true, 'available' => true },
      { 'username' => 'unavailable_coach', 'merge_request_coach' => true, 'available' => false },
      { 'username' => 'not_coach', 'merge_request_coach' => false, 'available' => true }
    ]
  end

  before do
    allow(WwwGitLabCom).to receive(:roulette).and_return(roulette)
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'help'

  describe '#applicable?' do
    let(:wider_community_command) { true }

    before do
      allow(subject).to receive(:wider_community_command?).with(for_gitlab_org: true, for_gitlab_com: true).and_return(wider_community_command)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org, or author is not from the wider community, or the comment is not from the resource author' do
      let(:wider_community_command) { false }

      include_examples 'event is not applicable'
    end

    it_behaves_like 'rate limited', count: 1, period: 86400
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment to reply to author and pings MR coaches' do
      body = <<~MARKDOWN.chomp
        Hey there @coach, could you please help @root out?
        /assign_reviewer @coach
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
