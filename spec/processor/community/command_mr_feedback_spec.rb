# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_feedback'

RSpec.describe Triage::CommandMrFeedback do
  include_context 'with slack posting context'
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: %(@gitlab-bot feedback),
        url: 'https://comment.url/note#123'
      }
    end
  end

  subject { described_class.new(event, messenger: messenger_stub) }

  before do
    allow(messenger_stub).to receive(:ping)
  end

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', 'feedback'
  it_behaves_like 'processor slack options', '#mr-feedback'

  describe '#applicable?' do
    let(:wider_community_command) { true }

    before do
      allow(subject).to receive(:wider_community_command?).with(for_gitlab_org: true, for_gitlab_com: true, by_resource_author: true).and_return(wider_community_command)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org, or author is not from the wider community, or the comment is not from the resource author' do
      let(:wider_community_command) { false }

      include_examples 'event is not applicable'
    end

    it_behaves_like 'rate limited Slack message posting', count: 1, period: 86400
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it_behaves_like 'slack message posting' do
      let(:message_body) do
        {
          text: <<~MARKDOWN,
            Hola MR coaches, a contributor has left feedback about their experience in #{event.url}.
          MARKDOWN
          attachments: [{ text: event.new_comment }]
        }
      end
    end
  end
end
