# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/command_mr_request_review'

RSpec.describe Triage::CommandMrRequestReview do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        new_comment: new_comment
      }
    end

    let(:labels) { '~"group::import"' }
    let(:new_comment) { %(#{Triage::GITLAB_BOT} ready) }
  end

  before do
    allow(Triage).to receive(:gitlab_org_group_member_usernames).and_return(%w[gl-reviewer1 gl-reviewer2])
  end

  subject { described_class.new(event) }

  it_behaves_like 'registers listeners', ['merge_request.note']
  it_behaves_like 'command processor', %w[ready review request_review] do
    let(:args_regex) { described_class::REVIEWERS_REGEX }
  end

  describe 'REVIEWERS_REGEX' do
    it { expect(described_class::REVIEWERS_REGEX).to eq(/@([^@ ]+)/) }
  end

  describe '#applicable?' do
    let(:wider_community_command) { true }

    before do
      allow(subject).to receive(:wider_community_command?).with(for_gitlab_org: true, for_gitlab_com: true).and_return(wider_community_command)
    end

    include_examples 'event is applicable'

    context 'when event project is not under gitlab-org, or author is not from the wider community, or the comment is not from the resource author' do
      let(:wider_community_command) { false }

      include_examples 'event is not applicable'
    end

    it_behaves_like 'rate limited', count: 1, period: 86_400
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    let(:label_action) { %(/label ~"#{described_class::WORKFLOW_READY_FOR_REVIEW_LABEL}") }

    context 'with no reviewers given' do
      it 'sets ~"workflow::ready for review"' do
        expect_comment_request(event: event, body: "#{label_action}\n") do
          subject.process
        end
      end
    end

    context 'with reviewers given' do
      shared_examples 'message posting' do |expected_reviewers:|
        it 'posts /assign_reviewer command' do
          body = <<~MARKDOWN.chomp
            #{label_action}
            /assign_reviewer #{Array(expected_reviewers).join(' ')}
          MARKDOWN

          expect_comment_request(event: event, body: body) do
            subject.process
          end
        end
      end

      context 'when reviewer is @gl-reviewer1' do
        let(:new_comment) { "#{Triage::GITLAB_BOT} ready @gl-reviewer1" }

        it_behaves_like 'message posting', expected_reviewers: '@gl-reviewer1'
      end

      context 'when command is not at the beginning of the comment' do
        let(:new_comment) { "Hello\n#{Triage::GITLAB_BOT} ready @gl-reviewer1\nWorld!" }

        it_behaves_like 'message posting', expected_reviewers: '@gl-reviewer1'
      end

      context 'with multiple reviewers on the same line with extra spaces' do
        let(:new_comment) { "#{Triage::GITLAB_BOT} ready   @gl-reviewer1    @gl-reviewer2" }

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1 @gl-reviewer2]
      end

      context 'with multiple commands on separate lines' do
        let(:new_comment) do
          <<~COMMENT
            Hello
            #{Triage::GITLAB_BOT} ready @gl-reviewer1
            #{Triage::GITLAB_BOT} ready @gl-reviewer2
          COMMENT
        end

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1]
      end

      context 'with a mix of allowed and disallowed reviewers' do
        let(:new_comment) do
          <<~COMMENT
            Hello
            #{Triage::GITLAB_BOT} ready @gl-reviewer1
            #{Triage::GITLAB_BOT} ready @not-gl-reviewer
          COMMENT
        end

        it_behaves_like 'message posting', expected_reviewers: %w[@gl-reviewer1]
      end
    end
  end
end
